# Hide Path

## INTRODUCTION

Provides an field formatter, who has hidden the path of images.
*Responsive images are supported.*

## REQUIREMENTS

This module requires the following modules:
 * Responsive Image (Core)

## INSTALLATION

Install as usual, see
https://drupal.org/documentation/install/modules-themes/modules-8
for further information.

## CONFIGURATION

  * Aktivate this module.
  * Go to the 'Hide Path' configuration page: `/admin/config/media/hipa`, and
  set a hash salt to encrypt the image URLs.

## MAINTAINERS

  Current maintainers:
   * Erik Seifert - https://www.drupal.org/user/161429
